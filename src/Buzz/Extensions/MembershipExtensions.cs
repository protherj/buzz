﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web.Security;
using umbraco.cms.businesslogic.member;

namespace Buzz.Extensions
{
    public static class MembershipUserExtensions
    {
        /// <summary>
        /// Returns the Umbraco member from the MembershipUser
        /// </summary>
        /// <param name="user"><see cref="MembershipUser"/></param>
        public static Member UmbracoMember(this MembershipUser user)
        {
            var member = new Member(int.Parse(user.ProviderUserKey.ToString()));
            return member;
        }
    }
}
