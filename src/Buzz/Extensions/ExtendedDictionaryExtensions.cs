﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Buzz.Models;

namespace Buzz.Extensions
{
    public static class DAMPImageExtensions
    {
        /// <summary>
        /// Adds additional non default serialized properties to the ExtendedData dictionary (eg a caption)
        /// </summary>
        /// <param name="di"><see cref="DampImage"/></param>
        /// <param name="xml">Serialized Xml</param>
        /// <param name="elementName">Custom property name</param>
        public static void ParseExtendedValue(this DampImage di, string xml, string elementName)
        {
            var xdoc = XDocument.Parse(xml);
            var el = xdoc.Descendants().FirstOrDefault(x => x.Name == elementName);
            if (el != null)
            {
                var key = XmlConvert.EncodeLocalName(el.Name.ToString());
                if (!di.ExtendedData.ContainsKey(key)) di.ExtendedData.Add(key, el.Value);
            }
        }
    }
}
