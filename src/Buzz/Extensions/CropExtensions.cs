﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Buzz.Common.Models;
using Buzz.Models;

namespace Buzz.Extensions
{
    public static class CropExtensions
    {
        /// <summary>
        /// Returns a crop from an enumeration of crops
        /// </summary>
        /// <param name="crops"></param>
        /// <param name="name">The name/alias of the crop set in the ImageCropper</param>
        public static Crop Get(this IEnumerable<Crop> crops, string name)
        {
            return crops.FirstOrDefault(x => x.Name == name);
        }
    }
}
