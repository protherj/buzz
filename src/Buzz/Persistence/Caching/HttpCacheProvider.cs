﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Buzz.Persistence.Caching
{
    /// <summary>
    /// Simple cache provider that manages http cache of custom code
    /// </summary>
    public class HttpCacheProvider<T> : CacheableAbstract
        where T : class
    {

        /// <summary>
        /// Gets and item from cache
        /// </summary>
        public static T Get(string key)
        {
            return Exists(key) ? (T)Cache[GenerateKey(key)] : null;
        }

        /// <summary>
        /// Retrieves a dictionary of cached items by cache key pattern
        /// </summary>        
        public static IDictionary<string, T> GetByPattern(string contains, string suffix = "buzz")
        {
            // assert the prefix is lower case
            contains = contains.ToLowerInvariant();

            // create a list of matches to remove from cache
            var itemsToReturn = new List<string>();
            var enumerator = Cache.GetEnumerator();

            while (enumerator.MoveNext())
            {
                // if the enumerating key matches the prefix - add it to the list to be purged
                if (enumerator.Key.ToString().ToLowerInvariant().Contains(contains) && enumerator.Key.ToString().ToLowerInvariant().EndsWith(suffix)) itemsToReturn.Add(enumerator.Key.ToString());
            }

            return itemsToReturn.ToDictionary(key => key, key => (T)Cache[key]);
        }

        /// <summary>
        /// Adds an item to cache
        /// </summary>
        public static void Add(string key, T value)
        {
            int cacheDurationMinutes;
            var success = int.TryParse(WebConfigurationManager.AppSettings["Buzz:DefaultCacheDurationMinutes"], out cacheDurationMinutes);

            if (!success) cacheDurationMinutes = 5;

            Cache.Insert(GenerateKey(key), value, null, DateTime.Now.AddMinutes(cacheDurationMinutes), TimeSpan.Zero);
        }

        /// <summary>
        /// Removes all SR Smith objects of type T
        /// </summary>
        public static void RemoveAllByType()
        {
            PurgeCacheItems(typeof(T).Name);
        }

        /// <summary>
        /// True/false indicating whether or not object is in cache
        /// </summary>
        /// <param name="key">Public key for cached object</param>
        public static bool Exists(string key)
        {
            return HttpContext.Current.Cache[GenerateKey(key)] != null;
        }

        /// <summary>
        /// Generates a unique cache key with respect to SR Smith objects
        /// </summary>        
        private static string GenerateKey(string key)
        {
            return string.Format("{0}-{1}-buzz", typeof(T).Name, key).ToLowerInvariant();
        }
    }
}
