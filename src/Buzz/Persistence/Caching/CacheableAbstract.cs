﻿using System.Collections.Generic;
using System.Web;

namespace Buzz.Persistence.Caching
{
    public abstract class CacheableAbstract
    {
        /// <summary>
        /// Quick reference to the Cache object
        /// </summary>
        protected static System.Web.Caching.Cache Cache
        {
            get { return HttpContext.Current.Cache; }
        }


        /// <summary>
        /// Removes all objects
        /// </summary>
        public static void RemoveAll()
        {
            PurgeCacheItems(string.Empty);
        }

        /// <summary>
        /// Removes an item from cache 
        /// </summary>
        public static void Remove(string key)
        {
            if (Cache[key] != null) Cache.Remove(key);
        }


        /// <summary>
        /// Purges the current http cache of items with keys
        /// </summary>
        /// <remarks>
        /// This is sort of like a wildcard purge of cache
        /// </remarks>
        public static void PurgeCacheItems(string contains, string suffix = "buzz")
        {
            // assert the prefix is lower case
            contains = contains.ToLowerInvariant();

            suffix = suffix.ToLowerInvariant();

            // create a list of matches to remove from cache
            var itemsToRemove = new List<string>();
            var enumerator = Cache.GetEnumerator();

            while (enumerator.MoveNext())
            {
                // if the enumerating key matches the prefix - add it to the list to be purged
                if (enumerator.Key.ToString().ToLowerInvariant().Contains(contains) && enumerator.Key.ToString().ToLowerInvariant().EndsWith(suffix)) itemsToRemove.Add(enumerator.Key.ToString());
            }

            // remove the items from cache
            foreach (var item in itemsToRemove)
            {
                Cache.Remove(item);
            }

        }
    }
}
