﻿using System.Collections.Generic;
using Buzz.Common.Models;
using Umbraco.Core.Dynamics;
using Umbraco.Web;
using Umbraco.Web.Models;
using Buzz.Models;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System;
using Umbraco.Core.Logging;
using Link = Buzz.Common.Models.Link;

namespace Buzz.Helpers
{
    /// <summary>
    /// Assists in retreiving / translating Umbraco property data
    /// </summary>
    public class DataTypeHelper
    {

        #region Related Link Picker

        /// <summary>
        /// Helper function that retrieves related link datatype property values
        /// </summary>
        /// <param name="context">UmbracoContext Current</param>
        /// <param name="relatedLinks"><see cref="Umbraco.Web.UmbracoContext"/> Current</param>
        /// <returns><see cref="System.Collections.Generic.List<T>"/> where T is helper class of RelatedLinks</returns>
        [Obsolete("Use the new IPublishedContent extension methods by referencing the Buzz.Common namespace")]
        public static List<Link> GetRelatedLinks(UmbracoHelper umbracoHelper, DynamicXml relatedLinksPropertyValue)
        {
            var links = new List<Link>();

            foreach (DynamicXml link in relatedLinksPropertyValue)
            {

                var rl = new Link()
                {
                    Title = link.BaseElement.Attribute("title").Value,
                    Target = link.BaseElement.Attribute("newwindow").Value == "0" ? "_self" : "_blank"
                };

                // internal or external link
                if (link.BaseElement.Attribute("type").Value == "internal")
                {
                    DynamicPublishedContent content = umbracoHelper.Content(int.Parse(link.BaseElement.Attribute("link").Value));
                    rl.Url = content.Url;
                }
                else
                {
                    rl.Url = link.BaseElement.Attribute("link").Value;
                }

                links.Add(rl);
            }

            return links;
        }

        #endregion

        #region MultiTextString

        /// <summary>
        /// Returns an enumerable list of strings intended to be populated from the MultipleTextString DataType
        /// </summary>
        /// <param name="current"><see cref="DynamicPublishedContent"/> content containing MutlipleTextString datatype</param>
        /// <param name="propertyAlias">Property alias of the MultipleTextString DataType</param>
        /// <returns><see cref="IEnumerable<string>"/></returns>
        [Obsolete("Use the new IPublishedContent extension methods by referencing the Buzz.Common namespace")]
        public static IEnumerable<string> GetMultiTextStrings(DynamicPublishedContent current, string propertyAlias)
        {
            var textstrings = new List<string>();

            if (current.HasValue(propertyAlias))
            {
                XDocument xdoc = XDocument.Parse(current.GetPropertyValue(propertyAlias));

                foreach(var text in xdoc.Descendants("value"))
                {
                    textstrings.Add(text.Value);
                }

            }
            return textstrings;
        }

        #endregion


        #region MultiNodeTreePicker

        /// <summary>
        /// Returns a list of <see cref="DynamicPublishedContent"/> from MNTP values
        /// </summary>
        /// <param name="context">The UmbracoContext</param>
        /// <param name="source"><see cref="DynamicPublishedContent"/> containing MNTP property</param>
        /// <param name="propertyAlias">The property alias of the MNTP property</param>
        /// <remarks>TODO : fix this so that we are not assuming the content type is Content</remarks>
        [Obsolete("Use the new IPublishedContent extension methods by referencing the Buzz.Common namespace")]
        public static IEnumerable<DynamicPublishedContent> GetMNTPContent(UmbracoContext context, DynamicPublishedContent source, string propertyAlias)
        {           
            if (source.HasProperty(propertyAlias) && source.HasValue(propertyAlias))
            {
                return GetMNTPContent(context, source.GetPropertyValue(propertyAlias));                
            }
            return new List<DynamicPublishedContent>();
        }

        /// <summary>
        /// Returns a list of <see cref="DynamicPublishedContent"/> from MNTP values
        /// </summary>
        /// <param name="context">The UmbracoContext</param>
        /// <param name="mntpXml">Serialized MNTP Xml</param>
        [Obsolete("Use the new IPublishedContent extension methods by referencing the Buzz.Common namespace")]
        public static IEnumerable<DynamicPublishedContent> GetMNTPContent(UmbracoContext context, string mntpXml)
        {
            var content = new List<DynamicPublishedContent>();

            var uHelper = new UmbracoHelper(context);

            foreach (var id in GetMNTPIds(mntpXml))
            {
                try
                {
                    content.Add((DynamicPublishedContent)uHelper.Content(id));
                }
                catch (Exception ex)
                {
                    LogHelper.Error(typeof(DataTypeHelper), "MNTP content with Id " + id.ToString() + " returned null - It is likely you need to republish so your local cache updates", ex);
                }
            }
            
            return content;
        }

        [Obsolete("Use the new IPublishedContent extension methods by referencing the Buzz.Common namespace")]
        public static int[] GetMNTPIds(DynamicPublishedContent current, string propertyAlias)
        {
            if (current.HasProperty(propertyAlias) && current.HasValue(propertyAlias))
            {
                return GetMNTPIds(current.GetPropertyValue(propertyAlias));
            }
           
            return new int[] {};
        }

        /// <summary>
        /// Returns an array of Ids saved by the MNTP
        /// </summary>
        /// <param name="mntpXml">mntpXml</param>
        [Obsolete("Use the new IPublishedContent extension methods by referencing the Buzz.Common namespace")]
        public static int[] GetMNTPIds(string mntpXml)
        {            
            return (XDocument.Parse(mntpXml).Descendants().Where(x => x.Name == "nodeId")).Select(x => int.Parse(x.Value)).ToArray();            
        }


        #endregion

        #region DAMP Media Picker

        /// <summary>
        /// Returns an enumeration of <see cref="DampImage"/> from serialized Xml from DAMP Media Picker DataType
        /// </summary>
        /// <param name="source"><see cref="DynamicPublishedContent"/> that contains the DAMP property</param>
        /// <param name="propertyAlias">The alias of the DAMP property</param>
        /// <param name="mediaTypeAlias">The MediaTypeAlias = defaults to Image</param>
        /// <param name="imageCropperAlias">The name of the image cropper - defaults to "imageCropper"</param>
        public static IEnumerable<DampImage> GetDAMPMediaImages(DynamicPublishedContent source, string propertyAlias, string mediaTypeAlias = "Image", string imageCropperAlias = "imageCropper")
        {
            if (source.HasProperty(propertyAlias) && source.HasValue(propertyAlias))
            {
                return GetDAMPMediaImages(source.GetPropertyValue(propertyAlias), mediaTypeAlias, imageCropperAlias);
            }
            else
            {
                return new List<DampImage>();
            }
        }

        /// <summary>
        /// Returns an enumeration of <see cref="DampImage"/> from serialized Xml from DAMP Media Picker DataType
        /// </summary>
        /// <param name="dampMediaItemXml">Xml serialized Xml from DAMP Media Picker</param>
        /// <param name="mediaTypeAlias">The MediaTypeAlias = defaults to Image</param>
        /// <param name="imageCropperAlias">The name of the image cropper - defaults to "imageCropper"</param>
        public static IEnumerable<DampImage> GetDAMPMediaImages(string dampMediaItemXml, string mediaTypeAlias = "Image", string imageCropperAlias = "imageCropper")
        {
            var dampImages = new List<DampImage>();

            XDocument xdoc = XDocument.Parse(dampMediaItemXml);

            foreach (XElement image in xdoc.Descendants(mediaTypeAlias))
            {
                var di = new DampImage()
                    {
                        Id = int.Parse(image.Attribute("id").Value),
                        ParentId = int.Parse(image.Attribute("parentID").Value),
                        Name = image.Attribute("nodeName").Value,
                        NodeTypeAlias = image.Attribute("nodeTypeAlias").Value,
                        Path = image.Attribute("path").Value,
                        UmbracoFile = image.Element("umbracoFile").Value,
                        UmbracoWidth = int.Parse(image.Element("umbracoWidth").Value),
                        UmbracoHeight = int.Parse(image.Element("umbracoHeight").Value),
                        UmbracoBytes = int.Parse(image.Element("umbracoBytes").Value),
                        UmbracoExtension = image.Element("umbracoExtension").Value,
                        Crops = !string.IsNullOrEmpty(image.Element(imageCropperAlias).ToString()) ? GetImageCropperCrop(image.Element(imageCropperAlias).ToString()) : new List<Crop>()                             
                    };
                dampImages.Add(di);
            }
            return dampImages;
        }

        /// <summary>
        /// Returns the crop from the first media item saved by the DAMP media picker
        /// </summary>
        /// <param name="current"><see cref="DynamicPublishedContent"/></param>
        /// <param name="propertyAlias">The alias of the DAMP property</param>
        /// <param name="cropName">The name of the crop to return</param>
        /// <param name="imageCropperAlias">The name of the image cropper - defaults to "imageCropper"</param>
        /// <returns></returns>
        public static string GetImageCropperUrl(DynamicPublishedContent current, string propertyAlias, string cropName, string mediaTypeAlias = "Image", string imageCropperAlias = "imageCropper")
        {
            var image = GetDAMPMediaImages(current, propertyAlias, mediaTypeAlias, imageCropperAlias).FirstOrDefault();

            var src = string.Empty;

            if (image != null)
            {
                var crop = image.Crops.Where(x => x.Name == cropName).FirstOrDefault();
                if (crop != null)
                {
                    return crop.Url;
                }
            }

            return src;            
        }

        #endregion


        #region Image Cropper

        /// <summary>
        /// Parses serialized Image Cropper xml for a list of crops
        /// </summary>
        /// <param name="cropperXml">Xml string</param>
        public static IEnumerable<Crop> GetImageCropperCrop(string cropperXml)
        {
            var crops = new List<Crop>();

            var xdoc = XDocument.Parse(cropperXml);
            foreach (var c in xdoc.Descendants().Where(x => x.Name == "crop"))
            {
                var crop = new Crop()
                    {
                        Name = c.Attribute("name").Value,
                        X = int.Parse(c.Attribute("x").Value),
                        Y = int.Parse(c.Attribute("y").Value),
                        X2 = int.Parse(c.Attribute("x2").Value),
                        Y2 = int.Parse(c.Attribute("y2").Value),
                        Url = c.Attribute("url").Value
                    };
                crops.Add(crop);
            }

            return crops;
        }

        #endregion

    }
}