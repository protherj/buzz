﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web;
using System.Globalization;

namespace Buzz.Helpers
{
    [Obsolete]
	public static class LanguageHelper
	{
		public static string GetFromDictionaryValue(UmbracoContext context, string translateValue)
		{
			var uHelper = new UmbracoHelper(context);

			var ti = CultureInfo.CurrentCulture.TextInfo;
			var dictionaryKey = ti.ToTitleCase(translateValue.Replace("&", " ").Replace("/", " ").Replace("-", " ")).Replace(" ", string.Empty);

			return string.IsNullOrEmpty(uHelper.GetDictionaryValue(dictionaryKey)) ? translateValue : uHelper.GetDictionaryValue(dictionaryKey);
		}
	}
}