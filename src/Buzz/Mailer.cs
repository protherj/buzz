﻿using System;
using System.Net.Mail;
using Buzz.Common.Models;
using Buzz.Common.Services;
using Umbraco.Core.Logging;

namespace Buzz
{
    [Obsolete("Use Buzz.Core.Services.EmailService")]
    public class Mailer
    {
        /// <summary>
        /// Sends the email and returns a string confirmation (true) or the error message from the exception generated
        /// </summary>
        [Obsolete("Use Buzz.Core.Services.EmailService.SendEmail")]
        public static string Send(IMailVariables mailVars)
        {
            return EmailService.SendEmail(mailVars);
        }

    }

}