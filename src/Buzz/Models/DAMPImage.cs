﻿using System.Collections.Generic;
using Buzz.Common.Models;

namespace Buzz.Models
{
    public class DampImage
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public DampImage()
        {
            ExtendedData = new Dictionary<string, string>();
        }

        /// <summary>
        /// The media Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The ParentId
        /// </summary>
        public int ParentId { get; set; }

        /// <summary>
        /// The NodeTypeAlias
        /// </summary>
        public string NodeTypeAlias { get; set; }

        /// <summary>
        /// The Path
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// The umbracoFile value
        /// </summary>
        public string UmbracoFile { get; set; }

        /// <summary>
        /// The umbracoWidth value
        /// </summary>
        public int UmbracoWidth { get; set; }

        /// <summary>
        /// The umbracoHeight value
        /// </summary>
        public int UmbracoHeight { get; set; }

        /// <summary>
        /// The umbracoBytes value
        /// </summary>
        public int UmbracoBytes { get; set; }

        /// <summary>
        /// The umbracoExtension value
        /// </summary>
        public string UmbracoExtension { get; set; }

        /// <summary>
        /// List of <see cref="Crop"/>
        /// </summary>
        public IEnumerable<Crop> Crops { get; set; }

        /// <summary>
        /// Any additional properties can be added to this dictionary via extension method
        /// </summary>
        public Dictionary<string, string> ExtendedData { get; internal set; }

        /// <summary>
        /// An alias to the UmbracoFile.  Designers work 
        /// </summary>
        public string Url { get { return UmbracoFile; } }

    }
}
