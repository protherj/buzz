﻿using Buzz.Common;

namespace Buzz.Web.Models
{
    /// <summary>
    /// Defines a search term
    /// </summary>
    public interface ISearchTerm
    {
        /// <summary>
        /// The search term
        /// </summary>
        string Term { get; set; }

        /// <summary>
        /// Defines how to the term should be considered
        /// </summary>
        SearchTermType SearchTermType { get; set; }
    }
}
