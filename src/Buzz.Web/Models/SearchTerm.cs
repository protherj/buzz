﻿using System.ComponentModel.DataAnnotations;
using Buzz.Common;

namespace Buzz.Web.Models
{
    public class SearchTerm
    {
        [Required]
        public string Term { get; set; }

        [Required]
        public SearchTermType SearchTermType { get; set; }
    }
}
