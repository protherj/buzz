﻿using System.Web.Mvc;
using Buzz.Common.Services;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace Buzz.Web.Controllers
{
    
    [PluginController("Buzz")]
    public class BuzzNavigationPluginSurfaceController : SurfaceController
    {

        /// <summary>
        /// Renders the main navigation
        /// </summary>      
        [ChildActionOnly]
        public ActionResult NavigationMain(
            string[] excludeDocumentTypes = null, 
            int maxLevels = 0, int parentId = 0, 
            bool includeContentWithoutTemplate = false, 
            string viewName = "")
        {
            var current = Umbraco.Content(UmbracoContext.PageId);
            
            DynamicPublishedContent start = null;
            if (parentId > 0)
            {
                start = Umbraco.Content(parentId);
            }
            
            if(start == null)
            {
                start = current.AncestorOrSelf(1);
            }

            var service = new NavigationService();

            var linkTier = service.BuildLinkTier(start, current, excludeDocumentTypes, start.Level, maxLevels, includeContentWithoutTemplate);

            if (string.IsNullOrEmpty(viewName))
                viewName = "BuzzNavigationMain";
            

            return PartialView(viewName, linkTier);
            
        }

        /// <summary>
        /// Renders a bread crumb navigation
        /// </summary>
        /// <param name="stopLevel">top level for the recursion</param>
        /// <param name="viewName">optional alternate view name</param>
        /// <returns></returns>
        [ChildActionOnly]
        public ActionResult BreadCrumbMenu(int stopLevel = 1, string viewName = "")
        {
            var current = Umbraco.Content(UmbracoContext.PageId);

            var service = new NavigationService();
            
            var breadcrumb = service.BuildBreadCrumb(stopLevel, current);

            if (string.IsNullOrEmpty(viewName))
                viewName = "BuzzBreadCrumbMenu";

            return PartialView(viewName, breadcrumb);
        }

    }
}
