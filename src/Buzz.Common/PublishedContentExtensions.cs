﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Buzz.Common.Models;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;


namespace Buzz.Common
{
    public static class PublishedContentExtensions
    {

        /// <summary>
        /// Builds a collection of links from values saved by the Umbraco RelatedLinks DataType
        /// </summary>
        public static IEnumerable<ILink> RelatedLinksToLinkList(this IPublishedContent source, UmbracoContext context, string propertyAlias)
        {

            var links = new List<ILink>();

            if (! source.MandateProperty(propertyAlias)) return links;
            
            var xdoc = XDocument.Parse(source.GetProperty(propertyAlias).Value.ToString());
           
            var umbraco = new UmbracoHelper(context);

            foreach (var xlink in xdoc.Descendants("link"))
            {
   
                var rl = new Link()
                {
                    Title = xlink.Attribute("title").Value,
                    Target = xlink.Attribute("newwindow").Value == "0" ? "_self" : "_blank"
                };

                // internal or external link
                if (xlink.Attribute("type").Value == "internal")
                {
                    var content = umbraco.Content(int.Parse(xlink.Attribute("link").Value));
                    rl.Url = content.Url;
                }
                else
                {
                    rl.Url = xlink.Attribute("link").Value;
                }

                links.Add(rl);
            }

            return links;
        }

        /// <summary>
        /// Builds a list of text based on values saved by an Umbraco MultiTextString DataType
        /// </summary>
        public static IEnumerable<string> MultiLineTextStringToList(this IPublishedContent source, string propertyAlias)
        {
            var texts = new List<string>();

            if (!source.MandateProperty(propertyAlias)) return texts;

            var xdoc = XDocument.Parse(source.GetProperty(propertyAlias).Value.ToString());

            texts.AddRange(xdoc.Descendants("value").Select(text => text.Value));

            return texts;
        }

        /// <summary>
        /// Creates a list of either content or media based on values saved by an Umbraco MultiNodeTreePicker DataType
        /// </summary>
        public static IEnumerable<IPublishedContent> MntpToPublishedContentList(this IPublishedContent source, UmbracoContext context, string propertyAlias, bool isMedia = false)
        {            
            if (!source.MandateProperty(propertyAlias)) return new List<IPublishedContent>();

            var ids = source.GetProperty(propertyAlias).Value.ToString().StartsWith("<MultiNodePicker")
                ? source.MntpXmlValuesToArray(propertyAlias)
                : source.MntpCsvValuesToArray(propertyAlias);

            var umbraco = new UmbracoHelper(context);

            return isMedia ? umbraco.Media(ids) : umbraco.Content(ids);            
        }

        /// <summary>
        /// Returns an array of Ids saved in Xml format by an Umbraco MultiNodeTreePicker DataType
        /// </summary>        
        public static int[] MntpXmlValuesToArray(this IPublishedContent source, string propertyAlias)
        {
            if (!source.MandateProperty(propertyAlias)) return new int[] {};

            return (XDocument.Parse(source.GetProperty(propertyAlias).Value.ToString()).Descendants().Where(x => x.Name == "nodeId")).Select(x => int.Parse(x.Value)).ToArray();
        }

        /// <summary>
        /// Returns an array of Ids saved in Xml format by an Umbraco MultiNodeTreePicker DataType
        /// </summary> 
        public static int[] MntpCsvValuesToArray(this IPublishedContent source, string propertyAlias)
        {
            if(!source.MandateProperty(propertyAlias)) return new int[] {};

            return source.GetProperty(propertyAlias).Value.ToString().Split(',').Select(int.Parse).ToArray();            
        }

        /// <summary>
        /// Asserts that a property exists and has a value
        /// </summary>
        /// <param name="source">The <see cref="IPublishedContent"/> source containing the property</param>
        /// <param name="propertyAlias">The alias or name of the property</param>
        /// <returns>True of false indicating whether or not the property exists and has a value</returns>
        public static bool MandateProperty(this IPublishedContent source, string propertyAlias)
        {
            return source.HasProperty(propertyAlias) && source.HasValue(propertyAlias);
        }
    }

}
