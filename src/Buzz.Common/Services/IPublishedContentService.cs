﻿namespace Buzz.Common.Services
{
    /// <summary>
    /// Defines a service that depends on Umbraco.Web.IPublishContent
    /// </summary>
    public interface IPublishedContentService : IService
    {
    }
}
