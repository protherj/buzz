﻿namespace Buzz.Common
{
    public enum SearchTermType
    {
        SingleWord,
        MultiWord
    }
}
