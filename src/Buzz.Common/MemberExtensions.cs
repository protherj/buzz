﻿using umbraco.cms.businesslogic.member;

namespace Buzz.Common
{
    
    public static class MemberExtensions
    {
        /// <summary>
        /// Sets a property value
        /// </summary>
        /// <param name="alias"></param>
        /// <param name="value">The property value</param>
        /// <param name="member"></param>
        public static void SetValue(this Member member, string alias, string value)
        {
            member.getProperty(alias).Value = value;
        }

        /// <summary>
        /// Returns the value of a property as a string
        /// </summary>
        /// <param name="member"></param>
        /// <param name="alias">The property alias</param>
        public static string GetPropertyValue(this Member member, string alias)
        {
            var prop = member.getProperty(alias);

            return prop != null ? prop.Value.ToString() : string.Empty;
        }
    }
}
