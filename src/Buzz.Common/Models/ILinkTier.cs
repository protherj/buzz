﻿using System.Collections.Generic;

namespace Buzz.Common.Models
{
    /// <summary>
    /// Defines a link tier
    /// </summary>
    public interface ILinkTier : ILink
    {
        List<ILinkTier> Children { get; set; }
    }
}
