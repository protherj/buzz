﻿namespace Buzz.Common.Models
{
    public class Crop : ICrop
    {
        public Crop() { }
        
        #region Implementation of ICrop

        public string Name { get; set; }

        public int X { get; set; }

        public int Y { get; set; }

        public int X2 { get; set; }

        public int Y2 { get; set; }

        public string Url { get; set; }

        #endregion

    }
}
