﻿namespace Buzz.Common.Models
{
    /// <summary>
    /// Hyperlink data
    /// </summary>
    public class Link : ILink
    {
        #region ILink Members

        public string Title { get; set; }

        public string Url { get; set; }

        public string Target { get; set; }

        public string ElementId { get; set; }

        public string CssClass { get; set; }

        #endregion
    }
}