﻿namespace Buzz.Common.Models
{
    /// <summary>
    /// Defines a crop
    /// </summary>
    public interface ICrop
    {
        /// <summary>
        /// The name of the crop
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Top left X value
        /// </summary>
        int X { get; set; }

        /// <summary>
        /// Top left Y value
        /// </summary>
        int Y { get; set; }

        /// <summary>
        /// Bottom right X value
        /// </summary>
        int X2 { get; set; }

        /// <summary>
        /// Bottom right Y value
        /// </summary>
        int Y2 { get; set; }

        /// <summary>
        /// The image url of the crop
        /// </summary>
        string Url { get; set; }

    }
}
