﻿using Buzz.Common.Models;
using Buzz.Common.Services;
using Umbraco.Core.Models;
using Umbraco.Web.Models;

namespace Buzz.Common
{
    public static class ContentExtensions
    {

        /// <summary>
        /// Builds a link tier using this IPublishedContent current as a reference point.
        /// </summary>
        public static ILinkTier BuildLinkTier(this IPublishedContent current, IPublishedContent start, 
            string[] excludeDocumentTypes = null,
            int tierLevel = 0,
            int maxLevel = 0,
            bool includeContentWithoutTemplate = false)
        {
            var navService = new NavigationService();

            return navService.BuildLinkTier(start, current, excludeDocumentTypes, tierLevel, maxLevel, includeContentWithoutTemplate);
        }

        /// <summary>
        /// Builds a link tier using this DynamicPublishedContent current as a reference point.
        /// </summary>
        public static ILinkTier BuildLinkTier(this DynamicPublishedContent current, IPublishedContent start, string[] excludeDocumentTypes = null,
            int tierLevel = 0,
            int maxLevel = 0,
            bool includeContentWithoutTemplate = false)
        {
            var navService = new NavigationService();

            return navService.BuildLinkTier(start, current, excludeDocumentTypes, tierLevel, maxLevel, includeContentWithoutTemplate);
        }
    }
}
